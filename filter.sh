#!/bin/bash

# This script filters the high frequency logged data to weekly peak usage excerpts.

# - peak open notes per day/week
# - peak active registered users per day/week
# - peak active non-registered users per week (non-registered) per day/week


# simple as this: store maximum for every distinguishable date and column, then output the result

cat stats/data.csv | gawk 'BEGIN{FS=",";}{
    if (substr($1,0,1)=="#") {
        for (i=2; i<=NF; i++)
        {
            # extract from 3 header lines:
            # 1) column title
            # 2) whether or not to plot this column (1/0)
            # 3) unit

            switch (headcounter) {
                case 0: header[i]=$i; break;
                case 1: printit[i]=$i; break;
                case 2:
                    # Add unit to the header text, if there is any unit, otherwise leave as-is.
                    if ($i!="")
                        header[i]=header[i] " [" $i "]";
                    break;
            }
            if (i>NFmax) NFmax=i;
        }
        headcounter++;

        # once we gathered all 3 headlines, we can print the newly constructed standard header
        if (headcounter==3) {
            printf("date") > "stats/data-daily.csv"
            printf("last date of week") > "stats/data-weekly.csv"
            for (i=1; i<=NFmax; i++) {
                if (printit[i]) {
                    if (i>1) {
                        printf(",") > "stats/data-daily.csv"
                        printf(",") > "stats/data-weekly.csv"
                    }
                    printf("\"peak %s\"",header[i]) > "stats/data-daily.csv"
                    printf("\"peak %s\"",header[i]) > "stats/data-weekly.csv"
                }
            }
            printf ("\n") > "stats/data-daily.csv"
            printf ("\n") > "stats/data-weekly.csv"
        }
    }
    else {
        # take the pure date as index (i.e. ignore time)
        date=substr($1,0,10)

        # first time run:
        if (!date_old)
            date_old=date;

        # whenever date changes: plot the current statistics for the previous date
        if (date!=date_old) {
            printf("%s",date_old) > "stats/data-daily.csv"
            # print, then delete data
            for (i=2; i<=NFmax; i++) {
                if (printit[i])
                {
                    printf(",%g",cache_daily[i]) > "stats/data-daily.csv"

                    # further cumulate maxima for multi-day / weekly statistics
                    if (cache_daily[i]>cache_weekly[i])
                        cache_weekly[i]=cache_daily[i];
                }
                cache_daily[i]=0;
            }
            printf("\n") > "stats/data-daily.csv"

            # multi-day / weekly statistics
            if (daycounter>=7) {
                printf("%s",date_old) > "stats/data-weekly.csv"
                for (i=2; i<=NFmax; i++) {
                    if (printit[i])
                    {
                        printf(",%g",cache_weekly[i]) > "stats/data-weekly.csv"
                    }
                    cache_weekly[i]=0;
                }
                printf("\n") > "stats/data-weekly.csv"
                daycounter=0;
            }
            daycounter++;

            NFmax=0;
            date_old=date;
        }

        # compare data for every line, store when appropriate (maximum)
        for (i=2; i<=NF; i++) {
            if ($i>cache_daily[i])
                cache_daily[i]=$i;
            if (i>NFmax)
                NFmax=i;
        }

    }

}'
