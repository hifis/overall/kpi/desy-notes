# Service Usage

* Usage statistics of DESY Notes
* <https://notes.desy.de>
* data source: <https://notes.desy.de/status>

## Plotting

* Time resolutions:
    - `stats/data.csv`: Fine-grained (every 15 min)
    - `stats/data-daily.csv`: Maximum per calendar day
    - `stats/data-weekly.csv`: Maximum per 7 days (week)

* Raw Data (bold face for further useful/further processed data):
    - **onlineNotes**,
    - **onlineUsers**,
    - distinctOnlineUsers,
    - **notesCount**,
    - **registeredUsers**,
    - **onlineRegisteredUsers**,
    - distinctOnlineRegisteredUsers,
    - isConnectionBusy,
    - connectionSocketQueueLength,
    - isDisconnectBusy,
    - disconnectSocketQueueLength

* Further processed data and explanations:

    - **peak onlineNotes:** The notes opened/edited simultaneously in the peak (on this day/week).  
    - **notesCount:** Total notes/documents stored. Number only decreases if notes were  deleted.  
    - **peak onlineRegisteredUsers:** The maximum number of registered users (i.e. via AAI) logged in at the same time at peak (on this day/week).  
    - **registeredUsers:** The total number of registered users, reduced only by deprovisioning.  
    - **peak onlineUsers:** The maximum number of users online at the same time, whether with or without AAI login (the latter anonymously).  

## Data + Weighing

Actual weighting ground truth can be found [here](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/blob/master/kpi-weights.csv?plain=1#L22).

| Data | Weighing |
| ----- | ----- |
| peak onlineNotes | 25% |
| peak online users | 16.7% |
| peak notes count | 25% |
| peak registered users | 16.7% |
| peak online egistered users | 16.7% |

## Schedule

* daily
* weekly
