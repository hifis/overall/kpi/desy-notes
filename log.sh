#!/bin/bash

# This script regularly requests for userstats of notes.desy.de (HedgeDoc) service
# and appends raw received data to log file.

logfile="stats/data.csv"

# write header if file does not exist yet (or is empty)
if [ ! -s $logfile ]; then
  echo -n "#date-time,"
  raw=$(curl --tlsv1.3 -s https://notes.desy.de/status | cut -d "{" -f 2 | cut -d "}" -f 1)
  for (( i=1; i<=11; i++ )); do
    col=$(echo $raw | cut -d "," -f $i | cut -d ":" -f 1 | cut -d "\"" -f 2)
    echo -n $col","
  done
  echo
  
  # the following lines are to be completed manually, if wanted
  # according to https://gitlab.hzdr.de/hifis/overall/kpi/kpi-requirements#file-content
  echo "#tbd" # one line for plot yes/no
  echo "#tbd" # unit for given column
fi >> $logfile


# endlessly logging
counter=0;
while [ true ]; do
  datetime=$(date --rfc-3339=seconds)
  {
  echo -n $datetime","
  raw=$(curl --tlsv1.3 -s https://notes.desy.de/status | cut -d "{" -f 2 | cut -d "}" -f 1)
  for (( i=1; i<=11; i++ )); do
    col=$(echo $raw | cut -d "," -f $i | cut -d ":" -f 2)
    echo -n $col","
  done
  echo
  } >> $logfile
  # push to repository once a day
  (( counter ++ ))
  if (( counter>= 1*24 )); then
    # before pushing: filter data
    ./filter.sh

    # now we go
    git commit -a -m 'update'
    git pull
    git push
    counter=0;
  fi

  sleep 60m
done
